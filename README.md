# Camera to display Engine 2

## Concept

on a H3/H5 Allwinner board, connect camera 1080P/15or30 ov5640/5647 on parallel CSI DVP bus.

Soft:

| FMT | Camera | IP | DMA | DE |
| ------ | ----- |---- | ---- | ---- |
| hw | OV5640 | suni6-csi | buffer | DE2 |

